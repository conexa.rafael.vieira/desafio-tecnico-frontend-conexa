import styled, { css } from 'styled-components';

export interface IPasswordProps {
	error?: boolean;
}

export const Container = styled.div`
	margin: 8rem 0;
	display: flex;
	justify-content: space-around;
	align-items: center;

	img {
		height: 525px;
	}

	@media (max-width: 768px) {
		img {
			display: none;
		}
	}
`;

export const Title = styled.h1`
	${({ theme }) => css`
		color: ${theme.colors.primaryBlue};
		text-align: center;
		margin-bottom: 3rem;
	`}
`;

export const DataWrapper = styled.div`
	label {
		margin-top: 1.5rem;
	}

	input {
		margin-top: 0.5rem;
	}

	button {
		margin-top: 3rem;
	}
`;

export const PasswordWrapper = styled.div<IPasswordProps>`
	position: relative;

	div:nth-of-type(2) {
		position: absolute;
		bottom: ${({ error }) => (error ? '4px' : '28px')};
		right: ${({ error }) => (error ? '6px' : '2px')};

		svg {
			width: 20px;
			height: 20px;
		}
	}
`;
