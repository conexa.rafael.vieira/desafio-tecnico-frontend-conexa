import { useState } from 'react';
import { Input } from 'components/Input';
import { Container, DataWrapper, PasswordWrapper, Title } from './styles';
import { Button } from 'components/Button';
import { Header } from 'components/Header';
import { useAuthContext } from 'context/Auth';
import { ILogin } from 'context/Auth/types';
import { Errors } from 'utils/validations/validationService';
import { userValidation } from 'utils/validations/validationLogin';
import { HiEyeOff, HiEye } from 'react-icons/hi';
import loginIlustration from 'assets/login.svg';

export const Login = () => {
	const { handleLogin } = useAuthContext();

	const [userData, setUserData] = useState<ILogin>({} as ILogin);
	const [formErrors, setFormErrors] = useState<Errors>({} as Errors);
	const [showPassword, setShowPassword] = useState(false);

	const toggleShowPassword = () => {
		setShowPassword(prevState => !prevState);
	};

	const handleForm = async () => {
		const errors = await userValidation(userData);
		setFormErrors({});

		if (errors) {
			setFormErrors(errors);
		} else {
			handleLogin(userData);
		}
	};

	return (
		<>
			<Header />
			<Container>
				<img src={loginIlustration} alt="Ilustração" />
				<DataWrapper>
					<Title>Faça Login</Title>
					<Input
						value={userData?.email || ''}
						name="text"
						label="E-mail"
						type="text"
						placeholder="Digite seu email"
						onChange={event => {
							setUserData({
								...userData,
								email: event.target.value,
							});
						}}
						error={formErrors?.email}
					/>
					<PasswordWrapper error={!formErrors?.password}>
						<Input
							value={userData?.password || ''}
							name="password"
							label="Senha"
							type={showPassword ? 'text' : 'password'}
							placeholder="Digite sua senha"
							onChange={event => {
								setUserData({
									...userData,
									password: event.target.value,
								});
							}}
							error={formErrors?.password}
						/>
						<div onClick={toggleShowPassword}>
							{showPassword ? <HiEye /> : <HiEyeOff />}
						</div>
					</PasswordWrapper>

					<Button variant="flat" size="large" onClick={handleForm}>
						Entrar
					</Button>
				</DataWrapper>
			</Container>
		</>
	);
};
