import { useEffect, useState } from 'react';
import { Header } from 'components/Header';
import { AppointmentList } from './components/AppointmentList';
import { Title, Wrapper } from './styles';
import { Button } from 'components/Button';
import { NewAppointment } from './components/NewAppointment';
import { ModalAppointment } from 'components/Modal';
import { useAuthContext } from 'context/Auth';
import { useConsultContext } from 'context/Consults';
import { NoAppointmentList } from './components/NoAppointmentList';

export const Consults = () => {
	const { user } = useAuthContext();
	const {
		isNewAppointmentModalOpen,
		consultList,
		setPatientData,
		setIsNewAppointmenModalOpen,
		getConsultList,
	} = useConsultContext();

	const [isStartAppointmentModalOpen, setIsStartAppointmentModalOpen] =
		useState(false);

	const toggleNewAppointmentModal = () => {
		setIsNewAppointmenModalOpen(prevState => !prevState);
		setPatientData(null);
	};

	const toggleStartAppointmentModal = () => {
		setIsStartAppointmentModalOpen(prevState => !prevState);
	};

	useEffect(() => {
		getConsultList();
	}, [getConsultList]);

	return (
		<>
			<Header hasButton />
			<Title>Olá, Dr (a) {user}.</Title>
			<Wrapper>
				<span>
					Você possui{' '}
					<span className="totalConsults">
						{consultList?.length > 0 ? consultList?.length : '0'}
					</span>{' '}
					{consultList?.length === 1 ? 'consulta' : 'consultas'}
				</span>
				<Button
					variant="flat"
					size="medium"
					onClick={toggleNewAppointmentModal}
				>
					Agendar consulta
				</Button>

				<NewAppointment
					isOpen={isNewAppointmentModalOpen}
					onRequestClose={toggleNewAppointmentModal}
				/>
			</Wrapper>

			{consultList?.length ? (
				<AppointmentList onClick={toggleStartAppointmentModal}>
					{consultList}
				</AppointmentList>
			) : (
				<NoAppointmentList />
			)}

			<ModalAppointment
				isOpen={isStartAppointmentModalOpen}
				onRequestClose={toggleStartAppointmentModal}
				title="Sua consulta já vai começar"
				handleLabelButton="Ir para o atendimento"
			>
				Você será redirecionado para o antedimento com o paciente em instantes.
			</ModalAppointment>
		</>
	);
};
