import { Button } from 'components/Button';
import { FormatDate, FormatTime } from 'utils/maskFullDate';
import { ConsultInfoWrapper, Container, DataWrapper } from './styles';

interface AppointmentListProps {
	children?: any;
	onClick?: React.MouseEventHandler<HTMLButtonElement>;
}

export const AppointmentList: React.FC<AppointmentListProps> = ({
	children,
	onClick,
}) => {
	return (
		<Container>
			{children?.map(item => (
				<DataWrapper key={item.id}>
					<ConsultInfoWrapper>
						<h2>
							{item.patient.first_name} {item.patient.last_name}
						</h2>
						<p>{item.patient.email}</p>
						<p>
							<span>Data do atendimento:</span> {FormatDate(item?.date)} às{' '}
							{FormatTime(item?.date)}
						</p>
					</ConsultInfoWrapper>
					<Button variant="flat" size="large" onClick={onClick}>
						Atender
					</Button>
				</DataWrapper>
			))}
		</Container>
	);
};
