import styled, { css } from 'styled-components';

export const Container = styled.div`
	margin: 2rem 3rem 3rem;
	display: grid;
	grid-template-columns: repeat(3, auto);
	align-items: center;
	gap: 24px;

	button {
		transition: all 0.2;
		&:hover {
			opacity: 0.85;
		}
	}

	@media (max-width: 1024px) {
		grid-template-columns: repeat(2, auto);
	}

	@media (max-width: 768px) {
		grid-template-columns: repeat(1, auto);
	}
`;

export const DataWrapper = styled.div`
	${({ theme }) => css`
		border: 2px solid ${theme.colors.disableBg};
		border-radius: 8px;
		width: 100%;
		height: auto;
		padding: 24px;
		cursor: pointer;
		user-select: none;

		button {
			margin-top: 42px;
		}
	`}
`;

export const ConsultInfoWrapper = styled.div`
	${({ theme }) => css`
		h2 {
			margin-bottom: 8px;
			width: 275px;
		}

		p {
			font-size: 18px;
			margin-bottom: 16px;

			span {
				color: ${theme.colors.primaryBlue};
				font-weight: 600;
				line-height: 30px;
			}
		}
	`}
`;
