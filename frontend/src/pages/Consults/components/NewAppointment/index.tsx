import { InputWrapper } from './styles';
import { Input } from 'components/Input';
import { ModalAppointment } from 'components/Modal';
import { useConsultContext } from 'context/Consults';
import { Autocomplete } from 'components/Autocomplete';
import { useEffect } from 'react';
import { toast } from 'react-toastify';

interface NewAppointmentProps {
	isOpen: boolean;
	onRequestClose: () => void;
}

export const NewAppointment: React.FC<NewAppointmentProps> = ({
	isOpen,
	onRequestClose,
}) => {
	const {
		patientData,
		patientList,
		setPatientData,
		createNewAppointment,
		getPatientList,
	} = useConsultContext();

	const handleNewAppointment = () => {
		try {
			createNewAppointment(patientData);
		} catch {
			toast.error('Não foi possível criar um atendimento. Tente novamente!', {
				theme: 'dark',
			});
		}
	};

	useEffect(() => {
		getPatientList();
	}, [getPatientList]);

	return (
		<ModalAppointment
			title="Novo atendimento"
			isOpen={isOpen}
			onRequestClose={onRequestClose}
			onConfirm={handleNewAppointment}
			handleLabelButton="Agendar"
			actionAvailable
			disabled={!patientData?.date ?? !patientData?.patientId}
		>
			<Autocomplete
				value={patientData?.patientId || ''}
				onChange={event => {
					setPatientData({
						...patientData,
						patientId: Number(event.target.value),
					});
				}}
				title="Nome do paciente"
				placeholder="Selecione o paciente"
			>
				<>
					<option value="" disabled>
						Selecione uma opção
					</option>
					{patientList?.map(item => (
						<option key={item.id} value={item.id}>
							{item.first_name} {item.last_name}
						</option>
					))}
				</>
			</Autocomplete>

			<InputWrapper>
				<Input
					name="datetime-local"
					label="Data do atendimento"
					type="datetime-local"
					placeholder="Selecione a data/hora"
					value={patientData?.date || ''}
					onChange={event => {
						setPatientData({
							...patientData,
							date: event.target.value,
						});
					}}
				/>
			</InputWrapper>
		</ModalAppointment>
	);
};
