import styled from 'styled-components';

export const InputWrapper = styled.div`
	label {
		padding-bottom: 0.5rem;
	}
	input {
		width: 100%;
	}
`;
