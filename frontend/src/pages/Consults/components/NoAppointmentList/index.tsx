import { Container, Title } from './styles';
import noSearchResults from 'assets/no-search-results.svg';

export const NoAppointmentList: React.FC = () => {
	return (
		<Container>
			<img src={noSearchResults} alt="Sem resultados" />
			<Title>
				Sem consultas agendadas no momento. Caso queira agendar uma nova
				consulta, ir no botão "Agendar consulta".
			</Title>
		</Container>
	);
};
