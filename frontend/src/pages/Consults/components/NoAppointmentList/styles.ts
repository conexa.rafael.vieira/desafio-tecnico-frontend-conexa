import styled, { css } from 'styled-components';

export const Container = styled.div`
	${({ theme }) => css`
		margin: 3rem;
		display: flex;
		align-items: center;
		flex-direction: column;
		justify-content: center;
		border: 2px solid ${theme.colors.disableBg};
		border-radius: 8px;
		padding: 24px;
	`}
`;

export const Title = styled.p`
	${({ theme }) => css`
		font-size: ${theme.fontSizes.large};
		margin-top: 24px;
		text-align: center;
		line-height: 32px;
	`}
`;
