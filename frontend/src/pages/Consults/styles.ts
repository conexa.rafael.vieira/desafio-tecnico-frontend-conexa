import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
	${({ theme }) => css`
		display: flex;
		justify-content: space-between;
		align-items: center;
		margin: 0 3rem 3rem;

		span {
			font-size: ${theme.fontSizes.xlarge};

			.totalConsults {
				color: ${theme.colors.primaryBlue};
				font-weight: 700;
			}
		}

		button {
			transition: all 0.2;

			&:hover {
				opacity: 0.8;
			}
		}
	`}
`;

export const Title = styled.h1`
	${({ theme }) => css`
		color: ${theme.colors.primaryBlue};
		margin: 3rem 3rem 1.5rem 3rem;
	`}
`;
