export interface ConsultProviderProps {
	children: React.ReactNode;
}

export interface IPatient {
	id: number | string;
	first_name: string;
	last_name: string;
	email: string;
}

export interface ICreateAppointment {
	patientId: number;
	date: string;
}

export interface IAppointmentList {
	id?: number;
	patientId: number;
	date: string;
	patient?: IPatient;
}

export interface IConsultContext {
	consultList: IAppointmentList[];
	patientData: ICreateAppointment;
	isNewAppointmentModalOpen: boolean;
	patientList: IPatient[];
	setPatientData: React.Dispatch<React.SetStateAction<ICreateAppointment>>;
	setIsNewAppointmenModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
	getConsultList: () => Promise<void>;
	createNewAppointment: (params: ICreateAppointment) => Promise<void>;
	getPatientList: () => Promise<void>;
}
