import { createContext, useCallback, useContext, useState } from 'react';
import { toast } from 'react-toastify';
import {
	ConsultProviderProps,
	IConsultContext,
	ICreateAppointment,
	IPatient,
} from './types';
import ConsultService from 'services/ConsultService';

export const ConsultContext = createContext<IConsultContext>(
	{} as IConsultContext,
);
export function ConsultProvider({ children }: ConsultProviderProps) {
	const [consultList, setConsultList] = useState(null);
	const [patientData, setPatientData] = useState<ICreateAppointment>(
		{} as ICreateAppointment,
	);
	const [patientList, setPatientList] = useState<IPatient[]>([]);
	const [isNewAppointmentModalOpen, setIsNewAppointmenModalOpen] =
		useState(false);

	const getConsultList = useCallback(async () => {
		try {
			const auth = JSON.parse(localStorage.getItem('@Conexa:token')!);
			const response = await ConsultService.getConsults(auth.token);
			setConsultList(response);
		} catch {
			toast.error('Ocorreu um problema. Tente novamente!', {
				theme: 'dark',
			});
		}
	}, []);

	const createNewAppointment = useCallback(
		async (params: ICreateAppointment) => {
			try {
				const auth = JSON.parse(localStorage.getItem('@Conexa:token')!);
				const response = await ConsultService.newAppointment(
					params,
					auth.token,
				);

				if (response.status === 201) {
					toast.success('Atendimento criado com sucesso!', { theme: 'dark' });
					getConsultList();
					setIsNewAppointmenModalOpen(false);
				}
			} catch {
				toast.error('Não foi possível criar um atendimento. Tente novamente!', {
					theme: 'dark',
				});
			}
		},
		[getConsultList],
	);

	const getPatientList = useCallback(async () => {
		try {
			const auth = JSON.parse(localStorage.getItem('@Conexa:token')!);
			const response = await ConsultService.getPatientList(auth.token);
			setPatientList(response);
		} catch {
			toast.error('Ocorreu um problema. Tente novamente!', {
				theme: 'dark',
			});
		}
	}, []);

	return (
		<ConsultContext.Provider
			value={{
				isNewAppointmentModalOpen,
				patientData,
				consultList,
				patientList,
				setIsNewAppointmenModalOpen,
				setPatientData,
				getConsultList,
				createNewAppointment,
				getPatientList,
			}}
		>
			{children}
		</ConsultContext.Provider>
	);
}

export const useConsultContext = (): IConsultContext => {
	const context = useContext(ConsultContext);
	if (!context) {
		throw new Error('useConsultContext must be used within a ConsultProvider');
	}
	return context;
};
