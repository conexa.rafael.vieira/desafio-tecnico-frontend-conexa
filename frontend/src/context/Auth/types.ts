export interface AuthProviderProps {
	children: React.ReactNode;
}

export interface ILogin {
	email: string;
	password: string;
}

export interface IResponse {
	name: string;
	token: string;
}

export interface IResponseLogin {
	status: number;
	data: IResponse;
}

export interface IAuthContext {
	authenticated: boolean;
	user: string;
	handleLogin: (params: ILogin) => Promise<void>;
	handleLogout(): void;
}
