import { createContext, useContext, useEffect, useState } from 'react';
import history from 'utils/history';
import { api } from 'services/api';
import { AuthProviderProps, IAuthContext, ILogin } from './types';
import { toast } from 'react-toastify';
import AuthService from 'services/AuthService';

export const AuthContext = createContext<IAuthContext>({} as IAuthContext);
export function AuthProvider({ children }: AuthProviderProps) {
	const [authenticated, setAuthenticated] = useState(false);
	const [user, setUser] = useState('');

	useEffect(() => {
		const auth = JSON.parse(localStorage.getItem('@Conexa:token')!);

		if (auth) {
			api.defaults.headers.common['Authorization'] = auth.token;
			setUser(auth.name);
			setAuthenticated(true);
		}
	}, []);

	const handleLogin = async (params: ILogin) => {
		try {
			const response = await AuthService.login(params);

			if (response.status === 200) {
				localStorage.setItem('@Conexa:token', JSON.stringify(response.data));
				setUser(response.data.name);
				api.defaults.headers.common['Authorization'] = response.data.token;
				setAuthenticated(true);
				history.push('/consultas');
			}
		} catch {
			toast.error('Credenciais incorretas. Tente novamente!', {
				theme: 'dark',
			});
		}
	};

	function handleLogout() {
		setAuthenticated(false);
		localStorage.removeItem('@Conexa:token');
		api.defaults.headers.common['Authorization'] = '';
		history.push('/');
	}

	return (
		<AuthContext.Provider
			value={{
				user,
				authenticated,
				handleLogin,
				handleLogout,
			}}
		>
			{children}
		</AuthContext.Provider>
	);
}

export const useAuthContext = (): IAuthContext => {
	const context = useContext(AuthContext);
	if (!context) {
		throw new Error('useAuthContext must be used within a AuthProvider');
	}
	return context;
};
