import { AuthProvider } from 'context/Auth';
import Routes from 'router';
import { ThemeProvider } from 'styled-components';
import theme from 'styles/theme';
import { GlobalStyle } from './styles/global';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ConsultProvider } from 'context/Consults';

function App() {
	return (
		<ThemeProvider theme={theme}>
			<AuthProvider>
				<ConsultProvider>
					<GlobalStyle />
					<Routes />
					<ToastContainer autoClose={4000} />
				</ConsultProvider>
			</AuthProvider>
		</ThemeProvider>
	);
}

export default App;
