import { useAuthContext } from 'context/Auth';
import { Consults } from 'pages/Consults';
import { Login } from 'pages/Login';
import { Switch, Route, Router, Redirect } from 'react-router-dom';
import history from 'utils/history';

interface CustomRoutesProps {
	isPrivate?: boolean;
}

const Routes = ({ isPrivate }: CustomRoutesProps) => {
	const { authenticated } = useAuthContext();
	return (
		<Router history={history}>
			<Switch>
				{isPrivate && !authenticated ? (
					<Redirect to="/" />
				) : (
					<>
						<Route exact path="/" component={Login} />
						<Route exact path="/consultas" component={Consults} />
					</>
				)}
			</Switch>
		</Router>
	);
};

export default Routes;
