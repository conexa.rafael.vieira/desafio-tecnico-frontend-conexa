import {
	IAppointmentList,
	ICreateAppointment,
	IPatient,
} from 'context/Consults/types';
import { api } from 'services/api';

class ConsultService {
	static async getConsults(token: string): Promise<IAppointmentList> {
		const { data } = await api.get('/consultations?_expand=patient', {
			headers: { Authorization: token },
		});

		return data;
	}

	static async newAppointment(
		params: ICreateAppointment,
		token: string,
	): Promise<any> {
		const { status, data } = await api.post('/consultations', params, {
			headers: { Authorization: token },
		});

		return { status, data };
	}

	static async getPatientList(token: string): Promise<IPatient[]> {
		const { data } = await api.get('/patients', {
			headers: { Authorization: token },
		});

		return data;
	}
}

export default ConsultService;
