import { ILogin, IResponseLogin } from 'context/Auth/types';
import { api } from 'services/api';

class AuthService {
	static async login(params: ILogin): Promise<IResponseLogin> {
		const { status, data } = await api.post('/login', params);

		return { status, data };
	}
}

export default AuthService;
