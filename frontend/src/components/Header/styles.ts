import styled, { css } from 'styled-components';

export const Container = styled.div`
	${({ theme }) => css`
		margin: 0 auto;
		padding: 1rem;
		display: flex;
		align-items: center;
		justify-content: space-between;
		background: ${theme.colors.gray};
	`}
`;
