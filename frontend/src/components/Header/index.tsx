import logoConexa from 'assets/logo-conexa.svg';
import { Button } from 'components/Button';
import { useAuthContext } from 'context/Auth';
import { Container } from './styles';

export interface HeaderProps {
	hasButton?: boolean;
}

export const Header = ({ hasButton }: HeaderProps) => {
	const { handleLogout } = useAuthContext();

	return (
		<Container>
			<nav>
				<img src={logoConexa} alt="Conexa Saúde" />
			</nav>

			{hasButton && (
				<Button variant="outlined" size="small" onClick={handleLogout}>
					Sair
				</Button>
			)}
		</Container>
	);
};
