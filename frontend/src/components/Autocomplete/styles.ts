import styled, { css } from 'styled-components';

export const Label = styled.label`
	${({ theme }) => css`
		font-size: ${theme.fontSizes.large};
		font-weight: 500;
		color: ${theme.colors.neutral};
		margin-bottom: 0.5rem;
	`}
`;

export const SelectContainer = styled.div`
	select {
		width: 100%;
		margin: 0.5rem 0 2rem 0;
		border-radius: 8px;
		height: 42px;
	}
`;
