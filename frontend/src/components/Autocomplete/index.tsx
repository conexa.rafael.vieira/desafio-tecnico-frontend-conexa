import { SelectContainer, Label } from './styles';

interface AutocompleteProps {
	onChange?: (event: React.ChangeEvent<HTMLSelectElement>) => void;
	value?: string | number;
	name?: string;
	title?: string;
	id?: string;
	placeholder?: string;
	children?: React.ReactElement | React.ReactElement[];
}

export const Autocomplete = ({
	onChange,
	id,
	value,
	name,
	title,
	children,
	placeholder,
}: AutocompleteProps) => {
	return (
		<div>
			<Label>{title}</Label>
			<SelectContainer>
				<select
					name={name}
					id={id}
					onChange={onChange}
					placeholder={placeholder}
					value={value}
				>
					{children}
				</select>
			</SelectContainer>
		</div>
	);
};
