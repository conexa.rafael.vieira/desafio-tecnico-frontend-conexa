import Modal from 'react-modal';
import closeImg from 'assets/close.svg';
import { ReactNode } from 'react';
import { ButtonWrapper, Container } from './styles';
import { Button } from 'components/Button';

interface ModalProps {
	isOpen: boolean;
	onRequestClose: () => void;
	onConfirm?: () => void;
	children?: ReactNode;
	title?: string;
	handleLabelButton?: string;
	actionAvailable?: boolean;
	disabled?: boolean;
}

export const ModalAppointment = ({
	isOpen,
	onRequestClose,
	onConfirm,
	children,
	title,
	handleLabelButton,
	actionAvailable,
	disabled,
}: ModalProps) => {
	return (
		<Modal
			isOpen={isOpen}
			onRequestClose={onRequestClose}
			overlayClassName="react-modal-overlay"
			className="react-modal-content"
			ariaHideApp={false}
		>
			<Container>
				<h2>{title}</h2>
				<button type="button" className="react-modal-close">
					<img src={closeImg} alt="Fechar modal" onClick={onRequestClose} />
				</button>
			</Container>
			{children}
			<ButtonWrapper>
				<Button
					variant="outlined"
					size={actionAvailable ? 'small' : 'medium'}
					onClick={onRequestClose}
				>
					Cancelar
				</Button>
				<Button
					variant="flat"
					size={actionAvailable ? 'small' : 'medium'}
					onClick={onConfirm}
					disabled={disabled}
				>
					{handleLabelButton}
				</Button>
			</ButtonWrapper>
		</Modal>
	);
};
