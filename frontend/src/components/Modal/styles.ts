import styled, { css } from 'styled-components';

export const Container = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
	margin-bottom: 24px;
`;

export const ButtonWrapper = styled.div`
	${({ theme }) => css`
		display: flex;
		justify-content: flex-end;
		align-items: center;
		margin-top: 4rem;
		gap: 16px;

		button {
			&:disabled {
				background: ${theme.colors.disableBg};
			}
		}
	`}
`;
