import { InputHTMLAttributes } from 'react';
import { Error, InputWrapper, Label } from './styles';

export type InputProps = {
	label?: string;
	error?: string;
	type?: 'password' | 'text' | 'datetime-local';
	name?: string;
	placeholder?: string;
	value?: string;
	onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
} & InputHTMLAttributes<HTMLInputElement>;

export const Input = ({
	label,
	error,
	type,
	name,
	placeholder,
	value,
	onChange,
	...props
}: InputProps) => {
	return (
		<div>
			<Label error={error}>{label}</Label>
			<InputWrapper
				error={error}
				type={type}
				name={name}
				placeholder={placeholder}
				value={value}
				onChange={onChange}
				{...props}
			/>
			{error && <Error>{error}</Error>}
		</div>
	);
};
