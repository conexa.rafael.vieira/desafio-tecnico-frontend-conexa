import styled, { css } from 'styled-components';
import { InputProps } from '.';

type ErrorProps = Pick<InputProps, 'error'>;

export const Error = styled.p`
	${({ theme }) => css`
		color: ${theme.colors.primaryDanger};
		font-size: ${theme.fontSizes.medium};
		font-family: 'Nunito';
		margin-top: 0.4rem;
	`}
`;

export const Label = styled.label<ErrorProps>`
	${({ theme, error }) => css`
		font-size: ${theme.fontSizes.large};
		font-weight: 500;
		display: flex;
		justify-content: flex-start;
		align-items: center;
		color: ${error ? theme.colors.primaryDanger : theme.colors.neutral};
	`}
`;

export const InputWrapper = styled.input<ErrorProps>`
	${({ theme, error }) => css`
		border: none;
		border-bottom: 2px solid
			${error ? theme.colors.primaryDanger : theme.colors.neutral};
		padding: 0.6rem 0;
		font-family: 'Nunito';
		font-weight: 300;
		font-size: ${theme.fontSizes.medium};
		width: 300px;
		background: ${theme.colors.background};

		&:focus {
			outline: 0;
		}
	`}
`;
