import { ReactNode } from 'react';
import { ButtonContainer } from './styles';

export type ButtonProps = {
	children: ReactNode;
	disabled?: boolean;
	variant?: 'outlined' | 'flat';
	size?: 'small' | 'medium' | 'large';
	onClick?: React.MouseEventHandler<HTMLButtonElement>;
};

export const Button = ({
	disabled = false,
	children,
	variant = 'outlined',
	size,
	onClick,
	...props
}: ButtonProps) => {
	return (
		<ButtonContainer
			{...props}
			disabled={disabled}
			variant={variant}
			size={size}
			onClick={onClick}
		>
			{children}
		</ButtonContainer>
	);
};
