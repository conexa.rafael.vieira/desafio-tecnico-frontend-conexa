import styled, { css } from 'styled-components';
import { ButtonProps } from '.';

type WrapperProps = Pick<ButtonProps, 'variant' | 'size'>;

export const ButtonContainer = styled.button<WrapperProps>`
	${({ variant, size }) => css`
		margin-top: 8px;
		padding: 4px;
		border: none;
		background-color: ${({ theme }) => theme.colors.primaryBlue};
		align-self: flex-end;
		color: ${({ theme }) => theme.colors.background};
		font-weight: 400;
		border-radius: 8px;

		${variant === 'outlined' &&
		css`
			background-color: ${({ theme }) => theme.colors.background};
			border: 2px solid ${({ theme }) => theme.colors.primaryBlue};
			color: ${({ theme }) => theme.colors.primaryBlue};
		`}

		${variant === 'flat' &&
		css`
			background-color: ${({ theme }) => theme.colors.primaryBlue};
			color: ${({ theme }) => theme.colors.background}; ;
		`}

		${size === 'small' &&
		css`
			width: 80px;
			height: 40px;
			font-size: 14px;
		`}

		${size === 'medium' &&
		css`
			width: 180px;
			height: 40px;
			font-size: 16px;
		`}

    ${size === 'large' &&
		css`
			width: 100%;
			height: 40px;
			font-size: 18px;
		`}
	`}
`;
