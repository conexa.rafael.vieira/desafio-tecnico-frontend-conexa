const FormatDate = (value: string) => {
	const date = new Date(value);

	return date.toLocaleString('pt-BR', {
		dateStyle: 'short',
	});
};

const FormatTime = (value: string) => {
	const time = new Date(value);

	return time.toLocaleTimeString('pt-BR', {
		hour: '2-digit',
		minute: '2-digit',
	});
};

export { FormatDate, FormatTime };
