import { ValidationError } from 'yup';

export type Errors = {
	[key: string]: string;
};

export const getValidationErrors = (err: ValidationError): Errors => {
	const validationErros: Errors = {};
	if (err) {
		err.inner.forEach(error => {
			if (error.path) {
				validationErros[error.path] = error.message;
			}
		});
	}
	return validationErros;
};
