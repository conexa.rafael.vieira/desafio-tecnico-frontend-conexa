import { ILogin } from 'context/Auth/types';
import * as Yup from 'yup';
import { getValidationErrors } from './validationService';

export const userValidation = async (values: ILogin) => {
	try {
		const schema = Yup.object().shape({
			email: Yup.string()
				.email('Informe um email válido.')
				.required('Campo obrigatório!'),
			password: Yup.string()
				.min(6, 'Sua senha deve ter no mínimo 6 caracteres.')
				.required('Campo obrigatório!'),
		});

		await schema.validate(values, { abortEarly: false });
	} catch (err) {
		if (err instanceof Yup.ValidationError) {
			return getValidationErrors(err);
		}
	}
};
