const theme = {
	colors: {
		primaryBlue: '#0031B2',
		primaryDanger: '#F64040',
		background: '#FFFFFF',
		neutral: '#4E535C',
		disableBg: '#DADEE2',
		gray: '#E5E5E5',
	},
	fontSizes: {
		small: '14px',
		medium: '16px',
		large: '18px',
		xlarge: '24px',
		big: '32px',
	},
};
export default theme;
