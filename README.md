# Conexa - Desafio Técnico Front-end

## Project Summary

Desenvolvido o desafio técnico front end Conexa (adaptado mais próximo  ao padrão da conexa atual), utilizando tecnologias como:

- React
- Typescript
- Styled-components
- React-toastify
- Yup
- Axios
- React-icons

# In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

# In the project directory, you can backend run:

### `Acess paste backend`

cd .. to `return` a directory and install deps with `yarn`

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3333](http://localhost:3333) to view it in the browser.
